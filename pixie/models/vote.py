from sqlalchemy import (
    Column,
    ForeignKey,
    Index,
    Integer,
    String,
    Text,
)

from .meta import Base


class Vote(Base):
    __tablename__ = 'votes'
    id = Column(Integer, primary_key=True)
    category = Column(Integer, ForeignKey("categories.id"), nullable=False)
    vote = Column(Text)
    ip = Column(String(63), nullable=False)
    user_agent = Column(Text)


Index('idx_vote_category', Vote.category, unique=False)
