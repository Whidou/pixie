from sqlalchemy import (
    Column,
    Enum,
    ForeignKey,
    Integer,
    UnicodeText,
)
import enum

from .meta import Base


class NotificationTypeEnum(enum.Enum):
    unknown = 0
    duplicate = 1
    nonexistent = 2
    language = 3
    date = 4
    other = 5


class Notification(Base):
    __tablename__ = 'notifications'
    id = Column(Integer, primary_key=True)
    type = Column(Enum(NotificationTypeEnum), nullable=False)
    candidate1_id = Column(Integer, ForeignKey("candidates.id"),
                           nullable=False)
    candidate2_id = Column(Integer, ForeignKey("candidates.id"))
    comment = Column(UnicodeText)
