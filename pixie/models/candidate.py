from sqlalchemy import (
    Column,
    Index,
    Integer,
    String,
    Text,
    UnicodeText,
)

from .meta import Base


class Candidate(Base):
    __tablename__ = 'candidates'
    id = Column(Integer, primary_key=True)
    categories = Column(String(15))
    title = Column(UnicodeText, nullable=False)
    link = Column(Text)
    image_url = Column(Text)
    description = Column(UnicodeText)


Index('idx_candidate_title', Candidate.title, unique=True, mysql_length=255)
