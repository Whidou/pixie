from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    UnicodeText,
)

from .meta import Base


class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    title = Column(UnicodeText, nullable=False)
    image_url = Column(Text)
    description = Column(UnicodeText)


Index('idx_category_title', Category.title, unique=True, mysql_length=255)
