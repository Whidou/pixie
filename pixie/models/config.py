from sqlalchemy import (
    Column,
    Index,
    Integer,
    String,
    UnicodeText,
)

from .meta import Base


class Config(Base):
    __tablename__ = 'config'
    id = Column(Integer, primary_key=True)
    variable = Column(String(31), nullable=False)
    value = Column(UnicodeText)


Index('idx_config_variable', Config.variable, unique=True, mysql_length=31)
