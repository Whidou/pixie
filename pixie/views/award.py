from pyramid.view import view_config


@view_config(route_name='home', renderer='../templates/award.jinja2')
def award_view(request):
    return {}
