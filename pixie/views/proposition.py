from pyramid.view import view_config
from pyramid.response import Response

import colander
import deform

from sqlalchemy.exc import DBAPIError

from .. import models


class PropositionSchema(deform.schema.CSRFSchema):

    title = colander.SchemaNode(
        colander.String(),
        title="Titre",
        description="Titre du jeu ou supplément candidat")

    category = colander.SchemaNode(
        colander.Integer(),
        title="Catégorie",
        description="Catégorie dans laquelle inscrire le candidat")


def add_candidate(db, candidates, title, category):
    candidate = next((c for c in candidates if c.title == title), None)
    if candidate is None:
        candidate = models.Candidate(categories=category, title=title)
        db.add(candidate)
        return

    category = str(category)
    if category not in candidate.categories:
        candidate.categories += str(category)


@view_config(route_name='proposition',
             renderer='../templates/proposition.jinja2')
def proposition_view(request):
    values = {}

    try:
        query = request.dbsession.query(models.Candidate)
        values["candidates"] = query.all()
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)

    try:
        query = request.dbsession.query(models.Category)
        values["categories"] = query.all()
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)

    cat_list = [("", "- Choisir une catégorie -")]
    for category in values["categories"]:
        cat_list.append((str(category.id), category.title))
    cat_list.sort(key=lambda x: x[0])

    schema = PropositionSchema().bind(request=request)
    schema['category'].widget = deform.widget.SelectWidget(values=cat_list)
    process_btn = deform.form.Button(name='send', title="Envoyer")
    form = deform.form.Form(schema, buttons=(process_btn,))
    values["form"] = form.render()

    if request.method == "POST" and 'send' in request.POST:
        try:
            appstruct = form.validate(request.POST.items())
            values["success"] = True
            values["title"] = appstruct["title"]
            values["category"] = cat_list[appstruct["category"]][1]
            add_candidate(request.dbsession, values["candidates"],
                          appstruct["title"], appstruct["category"])
        except deform.exception.ValidationFailure:
            values["success"] = False
            values["message"] = "données invalides"

    return values


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for descriptions and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
