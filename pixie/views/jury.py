from pyramid.view import view_config


@view_config(route_name='jury', renderer='../templates/jury.jinja2')
def jury_view(request):
    return {}
