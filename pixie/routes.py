def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view("static_deform", "deform:static")
    config.add_route('home', '/')
    config.add_route('jury', '/jury')
    config.add_route('manage', '/manage')
    config.add_route('presentation', '/presentation')
    config.add_route('proposition', '/proposition')
    config.add_route('vote', '/vote')
