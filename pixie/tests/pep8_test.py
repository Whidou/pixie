import unittest
from flake8.api import legacy as flake8


class PEP8Test(unittest.TestCase):
    def setUp(self):
        style_guide = flake8.get_style_guide(quiet=2)
        self.report = style_guide.check_files(["pixie"])

    def test_errors(self):
        errors = self.report.get_statistics('E')
        self.assertEqual(len(errors), 0, "\n".join(errors))

    def test_warnings(self):
        warnings = self.report.get_statistics('W')
        self.assertEqual(len(warnings), 0, "\n".join(warnings))
