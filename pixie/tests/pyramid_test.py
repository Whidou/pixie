import unittest

from pyramid import testing

import transaction


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('pixie.models')
        settings = self.config.get_settings()

        from pixie.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from pixie.models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from pixie.models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class TestProposition(BaseTest):
    def setUp(self):
        super().setUp()
        self.init_database()

        from pixie.models import Candidate, Category

        self.session.add(Category(title='Category 0'))
        self.session.add(Category(title='Category 1'))
        self.session.add(Candidate(title='Candidate 0', categories="1"))

    def test_passing_view(self):
        from pixie.views.proposition import proposition_view
        values = proposition_view(dummy_request(self.session))
        self.assertIn('Candidate 0', (c.title for c in values["candidates"]))
        self.assertIn('Category 0', (c.title for c in values["categories"]))
        self.assertIn('Category 1', (c.title for c in values["categories"]))
        self.assertTrue(values["form"])
        self.assertEqual(set(values),
                         set(("form", "candidates", "categories")))
